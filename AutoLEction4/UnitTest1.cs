﻿using AutoLEction4.Actions;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace AutoLEction4
{
    [TestFixture]
    public class UnitTest1
    {
        private IWebDriver Driver { get; set; }

        [TestFixtureSetUp]
        public void FixtureSetUp()
        {
            const string pathToChromedriver = @""; //Указать путь до Chromedriver

            Driver = new ChromeDriver(pathToChromedriver);
        }

        [Test]
        public void LoginTest()
        {
            var homePage = new LoginAction(Driver).DoLogin("login", "password"); //Указать логин и пароль
            
            Assert.That(homePage.UserName.Text, Is.EqualTo("Фамилия\r\nИмя Отчество"), "Авторизация не выполнена"); //Указать ФИО пользователя
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            Driver.Quit();
        }
    }
}
