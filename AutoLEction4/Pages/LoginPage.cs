﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace AutoLEction4.Pages
{
    class LoginPage
    {
        private readonly IWebDriver _driver;

        public LoginPage(IWebDriver driver)
        {
            _driver = driver;
        }

        public IWebElement LoginField
        {
            get
            {
                return _driver.FindElement(By.Id("login"));
            }
        }

        public IWebElement PasswordField
        {
            get
            {
                return _driver.FindElement(By.Id("password"));
            }
        }

        public IWebElement RemembermeCheckbox
        {
            get
            {
                return _driver.FindElement(By.Id("rememberme"));
            }
        }

        public IWebElement SubmitButton
        {
            get
            {
                return _driver.FindElement(By.ClassName("input_submit"));
            }
        }

        public void Open()
        {
            _driver.Navigate().GoToUrl(@"https://planeta.2gis.ru/login/");
        }
    }
}
