﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoLEction4.Pages;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace AutoLEction4.Actions
{
    class LoginAction
    {
        private readonly IWebDriver _driver;

        public LoginAction(IWebDriver driver)
        {
            _driver = driver;
        }

        public HomePage DoLogin(string login, string password)
        {
            var page = new LoginPage(_driver);

            page.Open();
            page.LoginField.SendKeys(login);
            page.PasswordField.SendKeys(password);
            page.RemembermeCheckbox.Click();
            page.SubmitButton.Click();

            return new HomePage(_driver);
        }
    }
}
